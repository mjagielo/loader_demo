package com.inspirethis.mike.loaderdemo.model;

/**
 * Created by mike on 1/11/16.
 */
public class Dog {

    private String breed;
    private String name;
    private String owner;
    private int age;
    private String location;

    public Dog (String breed, String name, String owner, int age, String location) {
        this.breed = breed;
        this.name = name;
        this.owner = owner;
        this.age = age;
        this.location = location;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
