package com.inspirethis.mike.loaderdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.inspirethis.mike.loaderdemo.model.Dog;
import com.inspirethis.mike.loaderdemo.model.DogAppDBAdapter;

import java.util.List;

public class MainActivity extends Activity /*implements LoaderManager.LoaderCallbacks<Cursor> */{

    private final String TAG = MainActivity.class.getSimpleName();
    private static final int LOADER_ID = 1932;
    private Handler handler = new Handler();

    private RecyclerView mRecyclerView;
    private SimpleCursorAdapter cursorAdapter;
    private DogListAdapter mAdapter;


    private List<Dog> mDogList;
//    private DogListAdapter mAdapter;
    private final DogAppDBAdapter mDatabase = DogAppDBAdapter.getDatabaseInstance(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.d("", "************ in onCreate");
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.list);

        mDatabase.open();
        mDogList = mDatabase.getAllDogs();

//        for (Dog d : mDogList)
//            Log.d("", "************ in onCreate, breed: " + d.getBreed());
//        Log.d("", "************ in onCreate, size: " + mDogList.size());
        mDatabase.close();

        mAdapter = new DogListAdapter(this, mDogList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

        Toast.makeText(this,"list size: " + mDogList.size(),Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
//        return new BasicLoader(this);
//    }
//
//    @Override
//    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
//        la.swapCursor(cursor);
//    }
//
//    @Override
//    public void onLoaderReset(Loader<Cursor> cursorLoader) {
//        la.swapCursor(null);
//    }
}
