package com.inspirethis.mike.loaderdemo.model;

import java.util.ArrayList;

/**
 * Created by mike on 1/11/16.
 */
public interface DogDBOperations {
    long addDog(Dog dog);
    void updateDog(final int id, final Dog updatedDog);
    void deleteDog(final Dog dog);
    Dog getDog(final int id);
    ArrayList<Dog> getAllDogs();
}
