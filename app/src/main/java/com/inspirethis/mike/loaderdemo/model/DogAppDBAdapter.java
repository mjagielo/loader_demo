package com.inspirethis.mike.loaderdemo.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by mike on 1/11/16.
 */
public class DogAppDBAdapter implements DogDBOperations{

    public static final String TAG = "DogAppDBAdapter";
    public static final String DOG_ROW_ID = "_id";
    public static final String DOG_BREED = "DogBreed";
    public static final String DOG_NAME = "DogName";
    public static final String DOG_AGE = "DogAge";
    public static final String DOG_LOCATION = "DogLocation";
    public static final String DOG_OWNER = "DogOwner";
    public static final String DOG_ID = "DogID";

    public static final String DATABASE_NAME = "Dog Application DB";
    private static final String DOG_T = "Dogs";
    private static final int DATABASE_VERSION = 1;

    public static DogAppDBAdapter databaseInstance;

    private MyDBHelper databaseHelper;

    private final Context context;

    private SQLiteDatabase database;

    public  static final Object[] dbLock = new Object[0];

    public static DogAppDBAdapter getDatabaseInstance(final Context context) {
        if (databaseInstance == null) {
            databaseInstance = new DogAppDBAdapter(context);
        }
        return databaseInstance;
    }

    private DogAppDBAdapter(Context context) {
        this.context = context;
        databaseHelper = new MyDBHelper(context);
    }

    public DogAppDBAdapter open() {
        databaseHelper = new MyDBHelper(context);
        try {
            database = databaseHelper.getWritableDatabase();
            Log.i("", " creating database: ");
        }catch(SQLiteException e) {
            Log.d("", "exception in creating database: " + e);
           // database = databaseHelper.getReadableDatabase();
        }
        return this;
    }

    public void close() {
        databaseHelper.close();;
    }

    @Override
    public long addDog(Dog dog) {

        ContentValues bucket = new ContentValues();
        bucket.put(DOG_BREED, dog.getBreed());
        bucket.put(DOG_NAME, dog.getName());
        bucket.put(DOG_AGE, dog.getAge());
        bucket.put(DOG_LOCATION, dog.getLocation());
        bucket.put(DOG_OWNER, dog.getOwner());

        return database.insert(DOG_T, null, bucket);
    }

    @Override
    public void updateDog(int id, Dog updatedDog) {
        //modifyDog(id, updatedDog);
        //Updates a specific tuple of Dog Table
        ContentValues updatedValues = new ContentValues();
        updatedValues.put(DOG_BREED, updatedDog.getBreed());
        updatedValues.put(DOG_NAME, updatedDog.getName());
        updatedValues.put(DOG_AGE, updatedDog.getAge());
        updatedValues.put(DOG_LOCATION, updatedDog.getLocation());
        updatedValues.put(DOG_OWNER, updatedDog.getOwner());
        String whereClause = DOG_ID + " = ?";
        String[] whereArgs = new String[]{Integer.toString(id)};

        database.update(DOG_T, updatedValues, whereClause, whereArgs);
    }


    @Override
    public void deleteDog(Dog dog) {
        String dogWhereClause = DOG_ID + " = ?";
        String[] dogWhereArgs = new String[1];
        database.delete(DOG_T, dogWhereClause, dogWhereArgs);
    }

    @Override
    public Dog getDog(int id) {
        String goalSelection = DOG_ID + " = ?";
        String[] dogSelectionArgs = new String[]{Integer.toString(id)};
        Cursor dogCursor = database.query(DOG_T, null, goalSelection, dogSelectionArgs, null, null, null);
        Dog extractedDog = null;
        if (dogCursor.moveToFirst()) {
            extractedDog = new Dog(dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_BREED)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_NAME)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_OWNER)),
                    dogCursor.getInt(dogCursor.getColumnIndexOrThrow(DOG_AGE)),
                    dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_LOCATION)));
        }

        return extractedDog;
    }


    @Override
    public ArrayList<Dog> getAllDogs() {
        ArrayList<Dog> dogs = new ArrayList<Dog>();
        Cursor dogCursor = database.query(DOG_T, null, null, null, null, null, null);
        Dog extractedDog = null;

        if (dogCursor.moveToFirst()) {
            for (dogCursor.moveToFirst(); !dogCursor.isAfterLast(); dogCursor.moveToNext()) {
                extractedDog = new Dog(dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_BREED)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_NAME)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_OWNER)),
                        dogCursor.getInt(dogCursor.getColumnIndexOrThrow(DOG_AGE)),
                        dogCursor.getString(dogCursor.getColumnIndexOrThrow(DOG_LOCATION)));
                dogs.add(extractedDog);
            }
        }
        return dogs;
    }

    private static class MyDBHelper extends SQLiteOpenHelper {
        public MyDBHelper(final Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE " + DOG_T + " (" +
                            DOG_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            DOG_BREED + " TEXT NOT NULL, " +
                            DOG_NAME + " TEXT NOT NULL, " +
                            DOG_OWNER + " TEXT NOT NULL, " +
                            DOG_AGE + " INTEGER, " +
                            DOG_LOCATION + " TEXT NOT NULL);"
            );

            buildDummyData(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase the_db, int oldVersion, int newVersion) {
            the_db.execSQL("DROP TABLE IF EXISTS " + DOG_T);
            onCreate(the_db);
        }

        private void buildDummyData(SQLiteDatabase db) {
            Log.d("","in build dummy data: ......");
            ContentValues bucket = new ContentValues();
            bucket.put(DOG_BREED, "Labrador");
            bucket.put(DOG_NAME, "Max");
            bucket.put(DOG_OWNER, "Bart Simpson");
            bucket.put(DOG_AGE, 5);
            bucket.put(DOG_LOCATION, "New Zealand");
            db.insert(DOG_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOG_BREED, "German Shepard");
            bucket.put(DOG_NAME, "Duke");
            bucket.put(DOG_OWNER, "Sally Jones");
            bucket.put(DOG_AGE, 5);
            bucket.put(DOG_LOCATION, "New Brunswick");
            db.insert(DOG_T, null, bucket);

            bucket = new ContentValues();
            bucket.put(DOG_BREED, "Irish Setter");
            bucket.put(DOG_NAME, "Red");
            bucket.put(DOG_OWNER, "Cleve Borderdash");
            bucket.put(DOG_AGE, 5);
            bucket.put(DOG_LOCATION, "New Jersey");
            db.insert(DOG_T, null, bucket);

        }
    }


}
